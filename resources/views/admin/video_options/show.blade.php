<table class='table table-striped table-bordered'>
	<tr>
		<th>Autoplay</th>
		<td>{{$data->autoplay}}</td>
	</tr>
	<tr>
		<th>Loop</th>
		<td>{{$data->loop}}</td>
	</tr>
	<tr>
		<th>Controls</th>
		<td>{{$data->controls}}</td>
	</tr>
	<tr>
		<th>Mute</th>
		<td>{{$data->mute}}</td>
	</tr>
	<tr>
		<th>Fullscreen</th>
		<td>{{$data->fullscreen}}</td>
	</tr>
</table>
<a href='{{route("adminVideoOptionsView", [$data->id])}}' class='btn btn-primary'>More Details</a>